﻿//Console是一个类，用于承载其中的方法、属性等内容。WriteLine是Console类的一个方法，用于在控制台中新显示一行文字“Hello， World！”，“”用于表示里面是字符串
//每个语句后必须加上英文分号;用于表示语句写完了，不然IDE会报错
Console.WriteLine("Hello, World!");
